import numpy as np
import logging

from cart.evaluation_functions import gini
from utils import get_logger
from .node import Node

log = get_logger("CART")

class CART:

	def __init__(self, max_depth = 2, evaluate_function=gini, verbose=False) -> None:
		if verbose:
			log.setLevel(logging.DEBUG)
		else:
			log.setLevel(logging.WARN)

		self.logger = log

		self.max_depth = max_depth
		self.evaluate_function = evaluate_function

	def fit(self, x, y) -> None:
		self.logger.debug(f"max depth = {self.max_depth}")
		self.logger.debug(f"Size of X = {len(x)}.")
		self.logger.debug("Fit X start...")

		self.n_classes = len(set(y))
		self.n_features = x.shape[1]

		self.logger.debug("Starting build solve tree...")
		self.tree = self._grow_tree(x, y)

	def predict(self, x):
		return [self._predict(inputs) for inputs in x]

	def _predict(self, inputs):
		node = self.tree
		while node.left:
			if inputs[node.feature_index] < node.threshold:
				node = node.left
			else:
				node = node.right
		return node.predicted_class

	def _grow_tree(self, x, y, depth = 0):
		count_of_objects_per_class = [np.sum(y == i) for i in range(self.n_classes)]
		predicted_class = np.argmax(count_of_objects_per_class)

		node = Node(
			evaluation_coefficient=self.evaluate_function(y),
			count_of_objects=y.size,
			count_of_objects_per_class=count_of_objects_per_class,
			predicted_class=predicted_class,
		)


		if depth < self.max_depth:
			idx, thr = self._best_split(x, y)

			if idx is not None:
				indices_left = x[:, idx] < thr
				x_left, y_left = x[indices_left], y[indices_left]
				x_right, y_right = x[~indices_left], y[~indices_left]

				node.feature_index = idx
				node.threshold = thr
				
				self.logger.debug(f"<depth={depth}, impurity={node.evaluation_coefficient}, feature_index={node.feature_index}, threshold={node.threshold}, predicted_class={node.predicted_class}>")

				node.left = self._grow_tree(x_left, y_left, depth + 1)
				node.right = self._grow_tree(x_right, y_right, depth + 1)

				if depth == 0:
					self.logger.debug("Tree has been build.")
					
		return node

	def _best_split(self, x, y):
		self.logger.debug("Searching best split...")
		counts_of_objects = len(y)

		if counts_of_objects <= 1:
			return None, None

		best_evaluation_coefficent = self.evaluate_function(y)
		best_idx, best_thr = None, None

		for idx in range(self.n_features):
			thresholds, classes = zip(*sorted(zip(x[:, idx], y)))
			for count_of_objects_in_left in range(1, counts_of_objects): # all possible split positions
				evaulation_coefficent_left = self.evaluate_function(classes[:count_of_objects_in_left])
				evaluation_coefficent_right = self.evaluate_function(classes[count_of_objects_in_left:])

				evaluation_coefficent = (count_of_objects_in_left * evaulation_coefficent_left + (counts_of_objects - count_of_objects_in_left) * evaluation_coefficent_right) / counts_of_objects

				if thresholds[count_of_objects_in_left] == thresholds[count_of_objects_in_left - 1]:
					continue

				if evaluation_coefficent < best_evaluation_coefficent:
					best_evaluation_coefficent = evaluation_coefficent
					best_idx = idx
					best_thr = (thresholds[count_of_objects_in_left] + thresholds[count_of_objects_in_left - 1]) / 2 # midpoint

		self.logger.debug("Best split founded")
		return best_idx, best_thr