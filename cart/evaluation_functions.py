import numpy as np

def gini(y):
	"""Calculate GINI"""
	y = np.array(y)
	count_of_objects = len(y)
	_, counts = np.unique(y, return_counts=True)
	return 1.0 - sum((n / count_of_objects) ** 2 for n in counts)