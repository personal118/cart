class Node:
    def __init__(
            self,
            evaluation_coefficient,   	 # Коэф. однородности
            count_of_objects,			 # Кол-во объектов в узле
            count_of_objects_per_class,  # Кол-во обхектов для каждого класса
            predicted_class				 # Предсказанный класс
    ):
        self.evaluation_coefficient = evaluation_coefficient
        self.count_of_object = count_of_objects
        self.count_of_object_per_class = count_of_objects_per_class
        self.predicted_class = predicted_class

        self.feature_index = 0			# Индекс атрибута для сплита
        self.threshold = 0				# Значение атрибута для сплита
        self.left = None				# Левый узел
        self.right = None				# Правый узел
