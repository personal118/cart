
import pandas as pd
import numpy as np

def get_dataset_wine():
	df = pd.read_csv('datasets/Wine.csv')
	df_filtered =  df[['Wine','Color.int','Hue','OD','Proline']]
	X = df_filtered.drop('Wine', axis=1).to_numpy()
	y_true = df["Wine"].to_numpy()

	return X, y_true