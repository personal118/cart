import pandas as pd
import numpy as np

def get_dataset_iris():
	df = pd.read_csv('datasets/Iris.csv')
	df = df.drop("Id",axis=1)
	goal_field = 'Species'

	observations = df.drop([goal_field],axis=1).to_numpy()
	classes = np.sort(df[goal_field].unique())
	for index, c in enumerate(classes):
		df.loc[df[goal_field] == c, goal_field] = index
	classes	 = df[goal_field].unique()

	X = observations
	y_true = df[goal_field]	

	return X, y_true