from sklearn import metrics
from datasets import Iris
from datasets.Iris import get_dataset_iris
from datasets.Wine import get_dataset_wine
from utils import get_logger
import pandas as pd
import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from cart import CART
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report

def draw_data(X_test, y_test, name):
	tmp = pd.DataFrame(data=X_test)
	tmp['y'] = np.asarray(y_test)
	g = sns.pairplot(tmp, hue='y', markers='*')
	g.savefig(name)

def iris_experiment(max_depth, verbose=False, draw_plots=False):
	X, y = get_dataset_iris()

	print(len(X))

	X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.33, random_state=42)
	cartModel = CART(verbose=verbose, max_depth=max_depth)
	cartModel.fit(X_train,y_train)

	# TRAIN STATS
	y_pred = cartModel.predict(X_train)
	if  draw_plots:
		print('train stats')
		draw_data(X_train, y_pred, "Iris_pred_train.png")
		plt.clf()

	result = {}
	result["train"] = pd.DataFrame(classification_report(list(y_train), list(y_pred), output_dict=True)).transpose()

	# TEST STATS
	y_pred = cartModel.predict(X_test)
	if  draw_plots:
		print('test stats')
		draw_data(X_test, y_pred, "Iris_pred_test.png")
		plt.clf()

	result["test"] = pd.DataFrame(classification_report(list(y_test), list(y_pred), output_dict=True)).transpose()

	if verbose:
		print(result)

	return result

def wine_experiment(max_depth, verbose=False, draw_plots=False):
	X, y = get_dataset_wine()

	print(len(X))

	X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.33, random_state=42)
	cartModel = CART(verbose=verbose, max_depth=max_depth)
	cartModel.fit(X_train,y_train)

	# TRAIN STATS
	y_pred = cartModel.predict(X_train)
	if  draw_plots:
		print('train stats')
		draw_data(X_train, y_pred, "Wine_pred_train.png")
		plt.clf()

	result = {}
	result["train"] = pd.DataFrame(classification_report(list(y_train), list(y_pred), output_dict=True)).transpose()

	# TEST STATS
	y_pred = cartModel.predict(X_test)
	if  draw_plots:
		print('test stats')
		draw_data(X_test, y_pred, "Wine_pred_test.png")
		plt.clf()

	result["test"] = pd.DataFrame(classification_report(list(y_test), list(y_pred), output_dict=True)).transpose()
	return result

def change_max_depth(experiment_function, max_depth_max=10, verbose=False, draw_plots=False):
	data_metrics = []
	for max_depth in range(2, max_depth_max + 1):
		result = {}
		metrics = experiment_function(max_depth=max_depth, verbose=verbose, draw_plots=draw_plots)
		result['max_depth'] = max_depth

		result['train_f1_score'] = metrics["train"]["f1-score"]["accuracy"]
		result['train_recall'] = metrics["train"]["recall"]["accuracy"]
		result['train_precision'] = metrics["train"]["precision"]["accuracy"]

		result['test_f1_score'] = metrics["test"]["f1-score"]["accuracy"]
		result['test_recall'] = metrics["test"]["recall"]["accuracy"]
		result['test_precision'] = metrics["test"]["precision"]["accuracy"]

		data_metrics.append(result)

	return pd.DataFrame(data_metrics)

def find_optimal_max_depth():
	metrics_pd = change_max_depth(iris_experiment)

	metrics_pd.to_excel("iris_metrics.xlsx") 

	sns.lineplot(data=metrics_pd, x="max_depth", y="train_f1_score", legend="brief", label="train_f1_score")
	sns.lineplot(data=metrics_pd, x="max_depth", y="train_f1_score", legend="brief", label="train_recall")
	sns.lineplot(data=metrics_pd, x="max_depth", y="train_f1_score", legend="brief", label="train_precision")

	sns.lineplot(data=metrics_pd, x="max_depth", y="test_f1_score", legend="brief", label="train_f1_score")
	sns.lineplot(data=metrics_pd, x="max_depth", y="test_f1_score", legend="brief", label="train_recall")
	sns.lineplot(data=metrics_pd, x="max_depth", y="test_f1_score", legend="brief", label="train_precision")

	plt.savefig("iris_max_depth_change_plot.png")
	plt.clf()

	metrics_pd = change_max_depth(wine_experiment)

	metrics_pd.to_excel("wine_metrics.xlsx") 

	sns.lineplot(data=metrics_pd, x="max_depth", y="train_f1_score", legend="brief", label="train_f1_score")
	sns.lineplot(data=metrics_pd, x="max_depth", y="train_f1_score", legend="brief", label="train_recall")
	sns.lineplot(data=metrics_pd, x="max_depth", y="train_f1_score", legend="brief", label="train_precision")

	sns.lineplot(data=metrics_pd, x="max_depth", y="test_f1_score", legend="brief", label="train_f1_score")
	sns.lineplot(data=metrics_pd, x="max_depth", y="test_f1_score", legend="brief", label="train_recall")
	sns.lineplot(data=metrics_pd, x="max_depth", y="test_f1_score", legend="brief", label="train_precision")

	plt.savefig("wine_max_depth_change_plot.png")
	plt.clf()

def main():
	# find_optimal_max_depth()
	metrics = iris_experiment(max_depth=5, verbose=True, draw_plots=True)
	print(metrics)

if __name__ == "__main__":
	main()